﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Canvas : MonoBehaviour
{

    //クリックしたらUIが消えるようにしたい
    //一応ｄをキー入力しても消える

    void Update()
    {
        ClickPanel();
    }

    private void ClickPanel() //パネルをクリックした時の処理
    {
        if(Input.GetMouseButtonDown(0) || Input.anyKeyDown)//左クリックされた時の処理
        {
            this.gameObject.SetActive(false);
        }
    }
}
