﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering;


/// <summary>
/// 
/// </summary>
public class SubUIController : MonoBehaviour
{
    public List<Camera> cameras=new List<Camera>();//カメラ配列
    [SerializeField] Camera AppearanceCamera;
    [SerializeField] GameObject HouseModel;//モデルハウスオブジェクト
    RenderTexture rt;//レンダラーテクスチャ
    public GameObject subMenu;//サムネ格納用UIオブジェクト(Content)
    public List<GameObject> thumbnails;//サムネイル一覧

    
    private void Awake()
    {
        //生成時モデルハウスの中の全てのカメラを取得して配列に格納
        HouseModel.GetComponentsInChildren<Camera>(cameras);
    }
    
    void Start()
    {
        cameras.Insert(0, AppearanceCamera);
        //サムネイル生成
        CreateSubImages();
    }

    public void CreateSubImages()
    {
        foreach (Camera cam in cameras)//カメラループ
        {
            if (cam.tag != "UserMovableCamera")
            {
                cam.GetComponent<Camera>().backgroundColor = new Color32(49, 77, 121, 255);
            }

            GameObject thumbnailRawImage = (GameObject)Instantiate(Resources.Load("Prefabs/RawImage"));//プレハブからサムネ用RawImage生成
            Vector2 fixSize = CalcFix(cam.sensorSize, thumbnailRawImage.GetComponent<RectTransform>().rect.size);//サイズ計算
            Texture2D texture = new Texture2D((int)fixSize.x, (int)fixSize.y, TextureFormat.ARGB32, false, false);//テクスチャ定義

            rt = new RenderTexture((int)fixSize.x, (int)fixSize.y, 26, RenderTextureFormat.ARGB32);//レンダラーテクスチャ定義
            rt.Create();//レンダラーテクスチャ作成
            cam.targetTexture = rt;//カメラのtargetTextureを生成したレンダラーテクスチャに
            cam.Render();//レンダリング
            RenderTexture.active = rt;//
            texture.ReadPixels(new Rect(0, 0, rt.width, rt.height), 0, 0);//
            texture.Apply();//レンダラーテクスチャをテクスチャ2Dに変換

            cam.targetTexture = null;//カメラのtargetTexture解放
            thumbnailRawImage.GetComponentInChildren<Text>().text = cam.GetComponent<Text>().text;

            thumbnailRawImage.GetComponent<RawImage>().texture = texture;//サムネのTextureを生成したTexture2Dに
            thumbnailRawImage.GetComponent<RectTransform>().sizeDelta = fixSize;//サムネのRectのsizeを計算後サイズに
            thumbnailRawImage.GetComponent<CameraThumbnailBottun>().SetParameters(cam);//サムネの各挙動用変数設定
            thumbnailRawImage.transform.SetParent(subMenu.transform);//サムネの親変更
            thumbnails.Add(thumbnailRawImage);//リストに格納
            cam.enabled = false;//カメラオフ
        }
    }

    Vector2 CalcFix(Vector2 cameraSize,Vector2 baseSize)//サイズ計算
    {
        var widthRatio = baseSize.x / cameraSize.x;//横比
        var heigthRatio = baseSize.y /cameraSize.y;//縦比
        var ratio = Mathf.Min(widthRatio, heigthRatio);//比率の小さいほうを取得
        return new Vector2(cameraSize.x * ratio, cameraSize.y * ratio);//リサイズしてVector2でリターン
    }

}
