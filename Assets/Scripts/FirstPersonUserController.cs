﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class FirstPersonUserController : MonoBehaviour
{

    bool reverseRotateMode;
    public Camera UserCamera;
    [SerializeField] float moveSpeed=100.0f;
    [SerializeField] float rotateSpeed=100.0f;
    float zoomMax;
    float zoomMin;
    float MaxHeight;
    float MinHeight;
    CharacterController controller;
    Vector3 startPos;
    Vector3 startRotate;
    Vector3 cameraStartPos;
    public Slider zoomSlider;
    public Slider POVSlider;

    Quaternion QAngle;
    Ray angleRay;

    bool mouseUse = false;

    Ray ray;
    Ray center;
    IEnumerator cor;

    private void Awake()
    {
        //ズーム範囲をスライダーから取得
        zoomMax = zoomSlider.maxValue;
        zoomMin = zoomSlider.minValue;
        //視点の高さ範囲をスライダーから取得
        MaxHeight = POVSlider.maxValue;
        MinHeight = POVSlider.minValue;
    }

    private void Start() {
        startPos = gameObject.transform.position;//ユーザー初期位置保存
        controller =gameObject.GetComponent<CharacterController>();//Userのキャラクターコントローラー取得
        reverseRotateMode = false;//初期反転モード
        startRotate = gameObject.transform.localEulerAngles;//ユーザー初期角度保存
        cameraStartPos = UserCamera.transform.localPosition;//カメラ初期位置保存
        cor = ClickMoveCoroutine();//移動コルーチン定義
    }

    private void Update() { 
        UserRotate();//回転
        UserMoveByKey();//移動
        UserMoveByMouseClick();
        RotateController();//カメラ回転
        zoomController();//ズーム
        POVHeightController();//高さ変更
        rotateReset(false);//リセット呼び出し
    }

    //動くメソッド
    void UserMoveByKey(){

        if(Input.GetKey(KeyCode.UpArrow)|| Input.GetKey(KeyCode.W)){//矢印キー上入力中
            //前方下45%にレイを飛ばして判定取得
            var region=Physics.Raycast(gameObject.transform.position, UserCamera.transform.TransformDirection(Vector3.forward + Vector3.down),10);
            
            if (region)//判定があれば
            {
                Vector3 forward = UserCamera.transform.TransformDirection(Vector3.forward);//前方計算
                controller.SimpleMove(forward * Time.deltaTime * moveSpeed);//移動
            }
        }


        if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
        {//矢印キー下入力中
            //後方下45度に例を飛ばして判定取得
            var region = Physics.Raycast(gameObject.transform.position, UserCamera.transform.TransformDirection(Vector3.back + Vector3.down), 10);

            if (region)//判定があれば
            {
                Vector3 back = UserCamera.transform.TransformDirection(Vector3.back);//後方計算
                controller.SimpleMove(back * Time.deltaTime * moveSpeed);//移動
            }
        }

    }

    //クリックで移動するメソッド
    void UserMoveByMouseClick()
    {

        if (Input.GetMouseButtonDown(0))//左クリックしたとき
        {

            if (!EventSystem.current.IsPointerOverGameObject())//UI判定
            {
                var target=(Input.mousePosition.x - Screen.width/2)/ Screen.width;//マウス入力で方向距離float

                QAngle = Quaternion.AngleAxis(target*rotateSpeed, Vector3.forward);//forwardを軸に回転角計算

                if (Mathf.Abs(target)>0.4)//マウスクリックが端なら
                {
                    mouseUse = true;//マウス回転用bool
                    cor = ClickRotateCoroutine();//回転コルーチン指定
                    StartCoroutine(cor);//回転コルーチン呼び出し
                }
                else
                {
                    cor = ClickMoveCoroutine();//移動コルーチン指定
                    mouseUse = false;//マウス+キーボード想定
                    StartCoroutine(cor);//移動コルーチン呼び出し
                }

                target = Mathf.Sign(target);//方向の正負を取る
            }
        }

        Debug.DrawRay(ray.origin, ray.direction * 10, Color.red, 0);//デバッグレイ

        Debug.DrawRay(center.origin, center.direction * 10, Color.green, 0);//デバッグレイ

        if (Input.GetMouseButtonUp(0))//左クリック話したら
        {
                StopCoroutine(cor);//コルーチン停止   
        }
    }

    //移動コルーチン
    IEnumerator ClickMoveCoroutine()
    {
        yield return StartCoroutine(ClickRotateCoroutine());//回転コルーチン完了待ち
        while (true)
        {
            var region = Physics.Raycast(gameObject.transform.position, UserCamera.transform.TransformDirection(Vector3.forward + Vector3.down), 10);
            if (region)//判定があれば
            {
                controller.SimpleMove(ray.direction * Time.deltaTime * moveSpeed);//レイの方向に移動
            }
            yield return null;
            if (Input.GetMouseButtonUp(0))//左クリック離したらブレイク
            {
                break;
            }
        }
    }

    //回転コルーチン
    IEnumerator ClickRotateCoroutine()
    {
        // ray = Camera.main.ScreenPointToRay(Input.mousePosition);//クリックした方向にレイを飛ばす
        ray = Camera.main.ViewportPointToRay(new Vector3(Input.mousePosition.x / Screen.width, 0.5f, 0f));//クリックした方向にレイを飛ばす
        while (true)
        {

            center = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));//画面の中心方向にレイを飛ばす

            gameObject.transform.Rotate(new Vector3(0, QAngle.z*rotateSpeed*Time.deltaTime, 0));//回転角と速度と時間で回転させる

            yield return null;

            if (!mouseUse)//マウス+キーボードモードなら
            {
                //レイのy軸以外判定用変数
                var rdx = Mathf.Round(ray.direction.x * 10);
                var cdx = Mathf.Round(center.direction.x * 10);
                var rdz = Mathf.Round(ray.direction.z * 10);
                var cdz = Mathf.Round(center.direction.z * 10);

                //2つのレイのx,zが大体一致したらbreak
                if (rdx == cdx && rdz == cdz)
                {
                    break;
                }
            }
            else//完全マウスなら
            {
                if (Input.GetMouseButtonUp(0))//左クリック離したらbreak
                {
                    break;
                }
            }
        }
    }

    //その場でユーザーオブジェクトを回転するメソッド
    void UserRotate(){


        if(Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
        {//矢印キー右入力中
            UserCamera.transform.Rotate(0,1.0f*Time.deltaTime*rotateSpeed,0,Space.World);//回転
        }

        if(Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
        {//矢印キー左入力中
            UserCamera.transform.Rotate(0,-1.0f*Time.deltaTime*rotateSpeed,0,Space.World);//回転
        }
    }

    //右クリックでカメラをコントロールするメソッド
    void RotateController(){

        if(Input.GetMouseButton(1)){//右クリック入力中

            Vector3 MouseAxis;//マウス移動方向変数宣言

            if(!reverseRotateMode){//カメラ反転モードoff
                //マウス右でカメラ左を向く
                MouseAxis=new Vector3(Input.GetAxis("Mouse Y"),-Input.GetAxis("Mouse X"),0);
            }
            else{//カメラ反転モードon
                //マウス右でカメラ右を向く
                MouseAxis=new Vector3(-Input.GetAxis("Mouse Y"),Input.GetAxis("Mouse X"),0);
            }
            UserCamera.transform.Rotate(MouseAxis);//計算した方向にカメラを回転させる
            //回転後z軸ブレを強制的に修正
            UserCamera.transform.localEulerAngles = new Vector3(UserCamera.transform.localEulerAngles.x, UserCamera.transform.localEulerAngles.y, 0);

        }
    }

    //カメラの角度・位置リセット
    public void rotateReset(bool sw){
        if(Input.GetMouseButtonDown(2) || sw){//ホイールクリックor他メソッドからtrue呼び出し
            UserCamera.transform.localRotation=Quaternion.identity;//角度リセット
            UserCamera.transform.localPosition = cameraStartPos;//カメラ位置リセット
        }
    }

    //ズーム
    void zoomController(){

        if (!Input.GetKey(KeyCode.LeftControl))//左Ctrlキー入力中は(高さコントロール中のため)実行しない
        {
            float scroll = Input.GetAxis("Mouse ScrollWheel");//ホイールスクロール量
            Vector3 localPosition = UserCamera.transform.localPosition;//カメラの現在ローカル位置取得

            //ローカル位置z+スクロール量で範囲判定
            if (localPosition.z + scroll > zoomMax)//最大値超えたら
            {
                localPosition.z = zoomMax;//最大値
            }
            else if (localPosition.z + scroll < zoomMin)//最小値下回ったら
            {
                localPosition.z = zoomMin;//最小値
            }
            else//範囲内なら
            {
                localPosition.z += scroll;//ローカル位置z+スクロール量
            }

            UserCamera.transform.localPosition = localPosition;//カメラのz位置のみ変更
        }
        zoomSlider.value = UserCamera.transform.localPosition.z;//スライダーのvalue変更

    }

    //視点の高さ
    void POVHeightController()
    {
        if (Input.GetKey(KeyCode.LeftControl))//左Ctrlキー入力中のみ
        {
            float scroll = Input.GetAxis("Mouse ScrollWheel");//ホイールスクロール量
            Vector3 localPosition = UserCamera.transform.localPosition;//カメラの現在ローカル位置取得

            //ローカル位置y+スクロール量
            if (localPosition.y + scroll > MaxHeight)//最大値超えたら
            {
                localPosition.y = MaxHeight;//最大値
            }
            else if (localPosition.y + scroll < MinHeight)//最小値下回ったら
            {
                localPosition.y = MinHeight;//最小値
            }
            else//範囲内なら
            {
                localPosition.y += scroll;//ローカル位置y+スクロール量
            }

            UserCamera.transform.localPosition = localPosition;//カメラのy位置のみ変更
        }
        POVSlider.value = UserCamera.transform.localPosition.y;//スライダーのvalue変更
    }

    //反転モード変更(反転)
    public void CameraReverse()
    {
        reverseRotateMode= !reverseRotateMode;
    }

    //反転モード変更(指定)
    public void CameraReverse(bool sw)
    {
        reverseRotateMode = sw;
    }

    //ユーザーリセット
    public void UserReset()
    {
        gameObject.transform.position = startPos;//位置リセット
        gameObject.transform.localEulerAngles = startRotate;//ユーザー角度リセット
        UserCamera.transform.localPosition = cameraStartPos;//カメラ位置リセット
        UserCamera.transform.localRotation = Quaternion.identity;//カメラ角度リセット
    }
}

