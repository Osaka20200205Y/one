﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// ユーザーアクティブ状態時UI関連用コントローラー
/// </summary>
public class UIController : MonoBehaviour
{
    public GameObject User;
    public Camera UserCamera;
    public Slider zoomSlider;
    public Slider POVSlider;
    public Toggle RotateModeToggle;
    public GameObject Content;
    public GameObject UserUI;
    public GameObject CameraUI;
    ThumbnailImagesController thumbnailController;
    SubUIController subUIConroller;
    List<Camera> cameras;

    //各フィールド変数取得
    void Start()
    {
        subUIConroller = gameObject.GetComponent<SubUIController>();

        thumbnailController = Content.GetComponent<ThumbnailImagesController>();
        try
        {
            cameras = subUIConroller.GetComponent<SubUIController>().cameras;
        }
        catch
        {
            ;
        }
    }

    //UI(スライダー)フォーカスリセット
    private void Update()
    {
        if (Input.GetMouseButtonUp(0))//マウス左クリックが離れたら
        {
            EventSystem.current.SetSelectedGameObject(null);//フォーカスをnullに(スライダーからフォーカスを外す)
        }
    }

    //トグルのOnValueChangeで呼び出す
    public void CameraReverse()
    {
        User.GetComponent<FirstPersonUserController>().CameraReverse();//UserのFPUCからカメラ反転呼び出し
    }

    //カメラリセットボタンで呼び出す
    public void CameraReset()
    {
        User.GetComponent<FirstPersonUserController>().rotateReset(true);//UserのFPUCからカメラリセット呼び出し(mousebottundown(2))
    }

    //全てリセット(初期化ボタン)
    public void ResetStatus()
    {
        User.SetActive(true);//ユーザーのactiveをtrueに
        User.GetComponent<FirstPersonUserController>().UserReset();//ユーザーのFPUCから位置・カメラ初期化
        RotateModeToggle.isOn = false;//トグルをoffに
        User.GetComponent<FirstPersonUserController>().CameraReverse(false);//メソッド内の反転モードをoffに
        thumbnailController.IlluminateSwitchReset();//空オブジェクトを渡してUIサムネ点灯初期化
        foreach(Camera cam in cameras)//カメラループ
        {
            if (cam.tag != "UserMovableCamera")//非ユーザーカメラのみ
            {
                try
                {
                    cam.GetComponent<SubCameraController>().ResetStatus();//位置・角度初期化
                }
                catch
                {
                    ;
                }
            }
        }
        UserUI.SetActive(true);//UIをユーザー用に
        CameraUI.GetComponentInChildren<Toggle>().isOn = false;//カメラUIのトグルをoffに
        CameraUI.SetActive(false);//カメラUIをoffに
        
    }

    //ズームスライダー用
    public void ZoomController()
    {
        //カメラの位置をスライダーのvalueに合わせる
        UserCamera.transform.localPosition = new Vector3(0, UserCamera.transform.localPosition.y, zoomSlider.value);
        //スライダーのvalueをカメラの位置に合わせる
        zoomSlider.value = User.GetComponentInChildren<Camera>().transform.localPosition.z;
    }

    //視点スライダー用
    public void POVHeightController()
    {
        //視点の位置をスライダーのvalueにあわせる
        UserCamera.transform.localPosition=new Vector3(0, POVSlider.value, UserCamera.transform.localPosition.z);
    }

}

