﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/// <summary>
/// サムネ一覧用コントローラー
/// </summary>
public class ThumbnailImagesController : MonoBehaviour
{
    public GameObject Controller;//コントローラー
    public List<GameObject> thumbnails = new List<GameObject>();//サムネリスト
    public List<Camera> cameras;//カメラリスト
    //[SerializeField] Camera AppearanceCamera;(9/7 現在凍結

    private void Start()
    {
        thumbnails = Controller.GetComponent<SubUIController>().thumbnails;//サムネリストをコントローラーのSUCから取得
        cameras = Controller.GetComponent<SubUIController>().cameras;//カメラリストをコントローラーのSUCから取得
    }

    //カメラ切り替え
    public void CameraSwitching(Camera currentCamera)
    {
        // AppearanceCamera.GetComponent<HouseModelRotateinAppearance>().ResetStatus();(9/7 現在凍結
        foreach (Camera camera in cameras)//カメラリストと引数のカメラを照合
        {
            if (camera != currentCamera)//一致しないなら
            {
                camera.enabled = false;//カメラoff
            }
            else//一致したら
            {
                camera.enabled = true;//カメラon
                Controller.GetComponent<CameraUIController>().SetCamera(camera);//カメラセット
            }
        }
    }

    //選択中カメラの枠表示切替
    public void IlluminateSwitch(GameObject currentPoVCamera)//サムネイル
    {
        foreach (GameObject thumbnail in thumbnails)
        {
            if (currentPoVCamera != thumbnail)//渡されたサムネイルオブジェクトとリストが一致しなかったら
            {
                thumbnail.GetComponent<CameraThumbnailBottun>().currentPoV = false;//現在カメラをoffに
                thumbnail.GetComponentInChildren<Image>().color = Color.black;//枠を黒に
            }
        }
    }

    public void IlluminateSwitchReset()//サムネイル
    {
        foreach (GameObject thumbnail in thumbnails)
        {
            thumbnail.GetComponent<CameraThumbnailBottun>().currentPoV = false;//現在カメラをoffに
            thumbnail.GetComponentInChildren<Image>().color = Color.black;//枠を黒に
        }
    }

}