﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


/// <summary>
/// カメラで生成した下メニューサムネイル用クラス
/// </summary>
public class CameraThumbnailBottun : MonoBehaviour , IPointerEnterHandler, IPointerExitHandler ,IPointerClickHandler
{
    Vector3 cameraPosition;//カメラの位置
    [SerializeField]bool onUserPermission;//ユーザーが移動可能か
    Quaternion cameraRotation;//カメラの角度
    public bool currentPoV=false;//現在選択中のカメラか
    Camera currentCamera;//現在のカメラ
    GameObject User;//ユーザーオブジェクト
    ThumbnailImagesController hasParentController;//親オブジェクト(Content)にアタッチされているスクリプト


    void Awake()
    {
        User = GameObject.FindGameObjectWithTag("Player");//ユーザーオブジェクトをタグで検索して取得・格納
    }

    //サムネイルの各挙動用変数設定
    public void SetParameters(Camera cam)
    {
            currentCamera = cam;//現在カメラ
            SetPermission(cam);//ユーザー移動可能か判定メソッド呼び出し
            SetPosition(cam.transform.position);//カメラの位置メソッド呼び出し
            SetRotation(cam.transform.rotation);//カメラの角度メソッド呼び出し
    }

    public void SetPosition(Vector3 pos)//位置格納
    {
        if (onUserPermission)//ユーザー移動可能なら
        {
            //カメラの座標から真下にレイを飛ばす
            Ray ground = new Ray(pos, Vector3.down);
            RaycastHit hit;
            float posY;//距離一時変数
            Physics.Raycast(ground, out hit, 10);
            //地面からの距離計算してy座標代入
            //カメラの座標とユーザーオブジェクトの高さからユーザー移動座標yを計算
            posY = hit.collider.gameObject.transform.position.y + User.GetComponent<CharacterController>().height / 2 ;
            //計算したユーザー移動座標を格納
            cameraPosition = new Vector3(pos.x, posY, pos.z);
        }
    }


    public void SetRotation(Quaternion rotate)//カメラの角度を格納
    {
        cameraRotation = rotate;
    }

    //カメラのユーザー移動可不可
    public void SetPermission(Camera cam)
    {
        if (cam.tag == "UserMovableCamera")//カメラのタグがユーザー移動可用タグなら
        {
            onUserPermission = true;
        }
        else
        {
            onUserPermission = false;
        }
    }

    //ポインターがサムネイルにenterしたら
    public void OnPointerEnter(PointerEventData eventData)
    {
        //親オブジェクト(Content)からコントローラー取得
        hasParentController = gameObject.transform.parent.GetComponent<ThumbnailImagesController>();
        //サムネ枠の色を赤に変更
        gameObject.GetComponentInChildren<Image>().color = Color.red;
    }

    //ポインターがサムネからexitしたら
    public void OnPointerExit(PointerEventData eventData)
    {
        if (!currentPoV)//現在選択中のカメラでないなら
        {
            gameObject.GetComponentInChildren<Image>().color = Color.black;//枠を黒に
        }
    }

    //サムネイルクリックしたら
    public void OnPointerClick(PointerEventData eventData)
    {

        try
        {
            currentPoV = true;//現在カメラをtrueに
            //親オブジェクト(content)のIlluminateSwitch呼び出し(引数Object=サムネイル)
            hasParentController.IlluminateSwitch(gameObject);//※1

            if (onUserPermission)//ユーザー移動可能なら
            {
                User.SetActive(true);//ユーザーオブジェクトをアクティブに
                User.transform.position = cameraPosition;//ユーザーの位置変更
                User.transform.rotation = cameraRotation;//ユーザーのカメラ角度変更
                User.GetComponentInChildren<Camera>().transform.rotation = cameraRotation;//カメラの角度変更(高さズームそのままで向きだけ初期化)
                hasParentController.CameraSwitching(currentCamera);//他のカメラをすべてoffにする
            }
            else
            {
                User.SetActive(false);//ユーザーオブジェクトを非アクティブに
                hasParentController.CameraSwitching(currentCamera);//他カメラをすべてoffにする
            }
        }
        catch
        {
            ;//操作タイミングによっては※1がnullエラーを吐くのでその回避
        }
    }


}
