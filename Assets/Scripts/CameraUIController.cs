﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// カメラ用(ユーザー非アクティブ時)UI関連のコントローラー
/// </summary>
public class CameraUIController : MonoBehaviour
{
    Camera currentCamera=null;//現在選択中のカメラ
    public GameObject UserUI;//通常UI(ユーザーUI)
    public GameObject CameraUI;//カメラ用UI
    public Toggle rotateModeToggle;//トグル

    //カメラセット
    public void SetCamera(Camera cam)
    {
        //非ユーザー有効カメラ
        if (cam.tag != "UserMovableCamera")
        {
            currentCamera = cam;//カメラセット
            UserUI.SetActive(false);
            CameraUI.SetActive(true);//UI切り替え
            OnActive();//カメラ反転モードスイッチ
        }
        else
        {
            UserUI.SetActive(true);
            CameraUI.SetActive(false);//UI切り替え
        }
    }

    private void OnActive()
    {
        //UIのトグルとカメラのモードをあわせる
        if (rotateModeToggle.isOn)
        {
            currentCamera.GetComponent<SubCameraController>().RotateSwitch(true);
        }
        else
        {
            currentCamera.GetComponent<SubCameraController>().RotateSwitch(false);
        }
    }


    public void CameraReverse()//トグルのOnValueChangeで呼び出す
    {
        try
        {
            currentCamera.GetComponent<SubCameraController>().RotateSwitch();//現在カメラの反転モード変更
        }
        catch
        {
            ;//コンソールエラー除け
        }

    }

    //カメラ単体初期化
    public void CameraStateReset()
    {
        rotateModeToggle.isOn = false;//toggleのチェック外す
        try//appearanceCamera用エラー除け
        {
            currentCamera.GetComponent<SubCameraController>().ResetStatus();//現在カメラのリセット呼び出し
        }
        catch
        {
            ;//握りつぶし
        }
    }
   
}
