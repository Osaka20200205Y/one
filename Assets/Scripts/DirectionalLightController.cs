﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DirectionalLightController : MonoBehaviour
{
    const float OrdinalAngle = 55.0f;//中間(春分・秋分)の角度
    Quaternion localRotate;//ライトの初期角度

    [SerializeField]Light directionalLight;//ライト
    [SerializeField] Slider lightRoughlyTime;//スライダー
    private float lightRoughlyDefalt;

    // Start is called before the first frame update
    void Start()
    {
        directionalLight.transform.localEulerAngles = new Vector3(55, 0, 0);
        localRotate = directionalLight.transform.rotation;//ライトの初期角度
        lightRoughlyDefalt = lightRoughlyTime.value;
    }


    // Update is called once per frame
    public void LightRotateChanger()
    {
        //スライダーの値と軸から角度計算
        Quaternion rotate = Quaternion.AngleAxis(lightRoughlyTime.value,Vector3.up);

        //新しい角度計算
        var newRotate = localRotate * rotate;

        //角度代入
        directionalLight.transform.rotation = newRotate;

    }

    public void LightRotateReset()//ライト初期化
    {
        lightRoughlyTime.value = lightRoughlyDefalt;
        var newRotate = localRotate;
    }
}
