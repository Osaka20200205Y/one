﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

/// <summary>
/// 非ユーザーカメラをコントロールするクラス
/// </summary>

public class SubCameraController : MonoBehaviour
{
    [SerializeField]float speed;//カメラ移動速度
    Vector3 StartRotation;//カメラ初期角度
    Vector3 StartPosition;//カメラ初期位置
    [SerializeField]bool Horizontal;//水平移動可不可
    [SerializeField]bool Vertical;//垂直移動可不可
    [SerializeField]bool rotateMode = false;//カメラ反転モード

    private void Start()
    {
        StartPosition = gameObject.transform.position;//カメラ初期位置保存
        StartRotation = gameObject.transform.eulerAngles;//カメラ初期角度保存
    }


    private void Update()
    {
        //カメラがoffのものは移動させない
        if (gameObject.GetComponent<Camera>().enabled)
        {
            if (Horizontal)
            {
                MoveHorizontal();
            }
            if (Vertical)
            {
                MoveVertical();
            }
        }
    }


    private void MoveHorizontal()
    {
        if (rotateMode)//反転モードチェック
        {
            //矢印キー=オブジェクトの回転方向(見掛け)
            if (Input.GetKey(KeyCode.RightArrow))
            {
                gameObject.transform.RotateAround(Vector3.zero, Vector3.up, Time.deltaTime * -speed);
            }
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                gameObject.transform.RotateAround(Vector3.zero, Vector3.up, Time.deltaTime * speed);
            }

            if (Input.GetKey(KeyCode.D))
            {
                gameObject.transform.RotateAround(Vector3.zero, Vector3.up, Time.deltaTime * -speed);
            }
            if (Input.GetKey(KeyCode.A))
            {
                gameObject.transform.RotateAround(Vector3.zero, Vector3.up, Time.deltaTime * speed);
            }
        }
        else
        {
            //矢印キー=カメラの回転方向(見掛け)
            if (Input.GetKey(KeyCode.RightArrow))
            {
                gameObject.transform.RotateAround(Vector3.zero, Vector3.up, Time.deltaTime * speed);
            }
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                gameObject.transform.RotateAround(Vector3.zero, Vector3.up, Time.deltaTime * -speed);
            }

            if (Input.GetKey(KeyCode.D))
            {
                gameObject.transform.RotateAround(Vector3.zero, Vector3.up, Time.deltaTime * speed);
            }
            if (Input.GetKey(KeyCode.A))
            {
                gameObject.transform.RotateAround(Vector3.zero, Vector3.up, Time.deltaTime * -speed);
            }
        }
        
    }

    private void MoveVertical()
    {
        //回転軸取得
        var localRotateAxis = gameObject.transform.localRotation*Vector3.right;

        if (Input.GetKey(KeyCode.UpArrow))
        {
            if (gameObject.transform.localEulerAngles.x+Time.deltaTime * speed < 90)//移動制限(妥協)
            {
                gameObject.transform.RotateAround(Vector3.zero, localRotateAxis, Time.deltaTime * speed);//カメラの移動
            }

        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            if (gameObject.transform.localEulerAngles.x - Time.deltaTime * speed > StartRotation.x)//移動制限(妥協)
            {
                gameObject.transform.RotateAround(Vector3.zero, localRotateAxis, Time.deltaTime * -speed);//カメラの移動
            }
        }

        if (Input.GetKey(KeyCode.W))
        {
            if (gameObject.transform.localEulerAngles.x + Time.deltaTime * speed < 90)//移動制限(妥協)
            {
                gameObject.transform.RotateAround(Vector3.zero, localRotateAxis, Time.deltaTime * speed);//カメラの移動
            }

        }

        if (Input.GetKey(KeyCode.S))
        {
            if (gameObject.transform.localEulerAngles.x - Time.deltaTime * speed > StartRotation.x)//移動制限(妥協)
            {
                gameObject.transform.RotateAround(Vector3.zero, localRotateAxis, Time.deltaTime * -speed);//カメラの移動
            }
        }
    }

    //カメラの単体リセット
    public void ResetStatus()
    {
        gameObject.transform.position = StartPosition;//位置初期化
        gameObject.transform.eulerAngles = StartRotation;//角度初期化
        RotateSwitch(false);//反転モード初期化
    }

    public void RotateSwitch()//反転モード変更(反転)
    {
        rotateMode = !rotateMode;
    }
    
    public void RotateSwitch(bool sw)//反転モード変更(指定)
    {
        rotateMode = sw;
    }
}
