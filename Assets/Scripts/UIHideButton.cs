﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;

public class UIHideButton : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] float distance;
    [SerializeField] Vector3 direction=Vector3.zero;
    public GameObject hidePanel;
    [SerializeField] bool display;
    Vector3 startPos;   


    // Start is called before the first frame update
    void Start()
    {
        display = true;
        startPos = hidePanel.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Sequence seq = DOTween.Sequence();

        if (display)
        {
            seq.Append(
                hidePanel.GetComponent<RectTransform>().DOLocalMove(direction,1.0f).SetRelative(true))
                .AppendCallback(() => gameObject.GetComponentInChildren<Text>().text = "〇")
                .AppendCallback(()=> seq.Kill());
            display = false;
            
        }
        else
        {
            var targetPos = startPos - hidePanel.transform.position;
            seq.Append(
                hidePanel.GetComponent<RectTransform>().DOLocalMove(targetPos, 1.0f).SetRelative(true))
                .AppendCallback(() => gameObject.GetComponentInChildren<Text>().text = "×")
                .AppendCallback(() => seq.Kill());
            display = true;
        }
    }
}
