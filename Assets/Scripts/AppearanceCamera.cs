﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class AppearanceCamera : MonoBehaviour
{
    [SerializeField] float speed;//カメラ移動速度
    Quaternion StartRotation;//カメラ初期角度
    Vector3 StartPosition;//カメラ初期位置
    Ray nextPositionRay = new Ray();//移動座標方向計算用レイ
    [SerializeField] float distance;//カメラの中心距離(円移動計算用)
    [SerializeField] float limitAngle;//制限角度
    [SerializeField] float zoomMax;//ズーム最大値
    float zoomMin = 0;//ズーム最小値
    [SerializeField] float baseSpeed;//基本速度
    Camera appearanceCamera;//外観カメラ
    [SerializeField] bool rotateMode = false;//回転モード
    bool pararell = false;

    private void Start()
    {
        StartPosition = gameObject.transform.position;//カメラ初期位置保存
        StartRotation = gameObject.transform.rotation;//カメラ初期角度保存
        appearanceCamera = gameObject.GetComponentInChildren<Camera>();//カメラ宣言
        distance = Vector3.Distance(Vector3.zero, gameObject.transform.position);//距離取得
    }

    private void Update()
    {
        if (appearanceCamera.enabled)
        {
            RotateMove();//回転移動
            PallarelMove();//平行移動
            zoom();//ズーム
            CalcYAxisDistance();//速度計算
        }
    }


    private void RotateMove()
    {
        if (Input.GetMouseButton(1))
        {//右クリック入力中

            Vector3 MouseAxis;//マウス移動方向変数宣言

            MouseAxis = new Vector3(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"), 0);//マウス移動量取得
            MouseAxis.Normalize();//正規化
            MouseAxis = RotateModeCheck(MouseAxis);//反転チェック
            MouseAxis *= speed;//速度乗算

            var localAxis = gameObject.transform.localRotation * MouseAxis;//回転軸計算

            nextPositionRay = new Ray(Vector3.zero, gameObject.transform.position + localAxis);//移動先の方向計算用レイ

            var nextPosition = nextPositionRay.direction *distance;//移動先の座標計算

            if (nextPosition.y < distance * limitAngle)//限界角度制限処理(距離と移動先のy座標による計算)
            {
                var LeapToPosition = new Vector3(nextPosition.x,
                                         Mathf.Clamp(nextPosition.y, 0, distance),
                                         nextPosition.z);//移動先座標のy軸制限処理

                gameObject.transform.position = Vector3.Slerp(gameObject.transform.position,
                                                LeapToPosition,
                                                Time.deltaTime * speed);//移動先座標にスラープ移動
            }
            gameObject.transform.LookAt(Vector3.zero);//Vec.0Look

        }
    }
    
    void CalcYAxisDistance()//移動速度計算
    {
        var Ydis = Vector3.Distance(Vector3.zero, new Vector3(gameObject.transform.position.x, 0, gameObject.transform.position.z));//y軸との距離取得

        speed = Mathf.Clamp((Ydis / distance)*baseSpeed,5.0f,10.0f);//基本距離とy軸との距離を計算(下限上限設定)
    }


    void zoom()//ズーム
    {
        float scroll = Input.GetAxis("Mouse ScrollWheel") * 10;//ホイールスクロール量

        if (scroll!=0)//スクロール量が0のとき実行しない
        {

            Vector3 localPosition = appearanceCamera.transform.localPosition;//カメラの現在ローカル位置取得

            //ローカル位置z+スクロール量で範囲判定
            if (localPosition.z + scroll > zoomMax)//最大値超えたら
            {
                localPosition.z = zoomMax;//最大値
            }
            else if (localPosition.z + scroll < zoomMin)//最小値下回ったら
            {
                localPosition.z = zoomMin;//最小値
            }
            else//範囲内なら
            {
                localPosition.z += scroll;//ローカル位置z+スクロール量
            }

            appearanceCamera.transform.localPosition = localPosition;//カメラのz位置のみ変更
        }

    }

    void PallarelMove()//平行移動
    {

        if (Input.GetMouseButtonDown(0))
        {
            if (!EventSystem.current.IsPointerOverGameObject())//UI判定
            {
                pararell = true;
            }
        }

        if (Input.GetMouseButton(0))//左クリック中
        {
                if (pararell)
                {
                    Vector3 MouseAxis;//マウス移動方向変数宣言

                    MouseAxis = new Vector3(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"), 0);//マウス移動量取得
                    MouseAxis.Normalize();//正規化
                    MouseAxis = RotateModeCheck(MouseAxis);//反転チェック

                    MouseAxis /= Mathf.Clamp(appearanceCamera.transform.localPosition.z,1,10);
                    appearanceCamera.transform.localPosition += MouseAxis;//カメラ平行移動
                }
        }

        if (Input.GetMouseButtonUp(0))
        {
            pararell = false;
        }
    }


    public void ResetStatus()//リセット
    {
        gameObject.transform.position = StartPosition;//位置初期化
        gameObject.transform.rotation = StartRotation;//角度初期化
        appearanceCamera.transform.localPosition = Vector3.zero;//カメラのズームリセット
        RotateSwitch(false);//反転モード初期化
    }

    public void RotateSwitch()//反転モード変更(反転)
    {
        rotateMode = !rotateMode;
    }

    public void RotateSwitch(bool sw)//反転モード変更(指定)
    {
        rotateMode = sw;
    }


    private Vector3  RotateModeCheck(Vector3 MouseAxis)//反転チェック
    {
        if (rotateMode == true)//反転モードチェック
        {
            return new Vector3(-MouseAxis.x, -MouseAxis.y, 0);//反転ベクトルreturn
        }
        return MouseAxis;
    }
}
